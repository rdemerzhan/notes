# notes

 
### Interesting Bash Commands
```bash 

look <string> *<file>   # looks for string in beginning of each line in file or if no file provided looks in dictionary

nl <file>               # like cat but adds line numbers and has many other options

uniq <input>            # filter output

ntpdate -u <ntpserver>  # Make life easier by syncing time. logs....

chattr +/-i             # lock/unclock file

tac <file>              # cat by reversed

iotop                   # top for io

pstree                  # pretty format for running processes

wget/curl               # Good stuff

lshw                    # Hardware info

lsblk                   # Show block devices

iperf                   # perform network throughput tests, look at man pages.

wall <message>          # Broadcast message on server to all 

notify-send <TITLE> <BODY> # Create a notification

compgen -c              # Display all available bash commands




```
### Interesting Programs to install

cssh - cluster SSH to control multiple hosts at ones

ansible - when configure many hosts or repeating yourself.

ag - silver fish for pretty grep and may also be faster

retty/reptyr - attach/reattach processes running on other terminal. *Useful if you forgot to use screen at beginning.*

newsbeuter - rss reader

nagmon - for monitoring network assets

hnb - simple app for notes 